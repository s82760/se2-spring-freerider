package de.freerider.model;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import de.freerider.repository.CrudRepository;

public class Customer {

	private String id;
	private String lastName;
	private String firstName;
	private String contact;

	public enum Status{
		New,
		InRegistration,
		Active,
		Suspended,
		Deleted
	}

	private Status status;

	public Customer(String lastName, String firstName, String contact){
		this.id = null;
		setlastName(lastName);
		setfirstName(firstName);
		setcontact(contact);
		this.status = Status.New;
	}
	
	public Customer(){
		this.id = null;
		this.lastName = "";
		this.firstName = "";
		this.contact = "";
		this.status = Status.New;
	}

	public void setStatus(Status status){
			this.status = status;
	}

	public Status getStatus(){
		return status;
	}
	
	public void setId(String id){
		if(this.id == null || id == null) {
			this.id = id;
		}
	}

	public String getId(){
		return id;
	}

	public void setlastName(String lastName){
		if(lastName != null) {
			this.lastName = lastName;
		}
		else {
			this.lastName = "";
		}	
	}

	public String getlastName(){
			return lastName;    
	}

	public void setfirstName(String firstName){
		if(firstName != null) {
			this.firstName = firstName;
		}
		else {
			this.firstName = "";
		}	
	}

	public String getfirstName(){
			return firstName;
	}

	public void setcontact(String contact){
		if(contact != null) {
			this.contact = contact;
		}
		else {
			this.contact = "";
		}			
	}

	public String getcontact(){
		return contact;
	}
}
