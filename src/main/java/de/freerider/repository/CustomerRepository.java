package de.freerider.repository;

import org.springframework.stereotype.Component;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import de.freerider.model.Customer;

@Component
class CustomerRepository implements CrudRepository<Customer, String> {

	private final IDGenerator idGen = new IDGenerator("C", IDGenerator.IDTYPE.NUM, 6);
	Set<Customer> customer = new HashSet<>();

	@Override
	public <S extends Customer> S save(S entity) {
		if (entity == null) {
			throw new IllegalArgumentException();
		}
		if (entity.getId() == null || entity.getId() == "") {
			String id = idGen.nextId();
			while (existsById(id)) {
				id = idGen.nextId();
			}
			entity.setId(id);
		}
		Optional<Customer> found = findById(entity.getId());
		if (!found.isPresent()) {
			customer.add(entity);
		}
		else {
			S customerFound = (S) found.get();
			customer.remove(customerFound);
			customer.add(entity);
			return customerFound;
		}
		return entity;
	}

	@Override
	public <S extends Customer> Iterable<S> saveAll(Iterable<S> entities) {
		if (entities == null) {
			throw new IllegalArgumentException();
		}
		for (Customer c : entities) {
			save(c);
		}
		return entities;
	}

	@Override
	public Optional<Customer> findById(String id) {
		if (id == null) {
			throw new IllegalArgumentException();
		}
		Optional<Customer> opt = Optional.empty();
		for (Customer c : customer) {
			if (c.getId().equals(id)) {
				opt = Optional.of(c);
			}
		}
		return opt;
	}

	@Override
	public boolean existsById(String id) {
		if (id != null) {
			for (Customer c : customer) {
				if (c.getId().equals(id)) {
					return true;
				}
			}
		} else {
			throw new IllegalArgumentException();
		}
		return false;
	}

	@Override
	public Iterable<Customer> findAll() {
		return customer;
	}

	@Override
	public Iterable<Customer> findAllById(Iterable<String> ids) {
		if(ids == null) {
			throw new IllegalArgumentException();
		}
		Set<Customer> newcustomer = new HashSet<>();
		for (Customer c : customer) {
			for (String id : ids) {
				if (existsById(id)) {
					newcustomer.add(c);
				} else {
					throw new IllegalArgumentException();
				}
			}
		}
		return newcustomer;
	}

	@Override
	public long count() {
		return customer.size();
	}

	@Override
	public void deleteById(String id) {
		if (existsById(id)) {
			for (Customer c : customer) {
				if (c.getId().equals(id)) {
					delete(c);
				}
			}
		}
	}

	@Override
	public void delete(Customer entity) {
		if (entity != null) {
			if (entity.getId() == null) {
				throw new IllegalArgumentException();
			}
			customer.remove(entity);
		} else {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public void deleteAllById(Iterable<? extends String> ids) {
		Customer c = null;
		if (ids != null) {
			for (String cid : ids) {
				if (cid.equals(null)) {
					throw new IllegalArgumentException();
				}
				Optional<Customer> c2 = findById(cid);
				if (c2.isPresent()) {
					c = c2.get();
					delete(c);
				}
			}
		} else {
			throw new IllegalArgumentException();
		}

	}

	@Override
	public void deleteAll(Iterable<? extends Customer> entities) {
		if(entities == null) {
			throw new IllegalArgumentException();
		}
		for (Customer c : entities) {
			if (c != null) {
				delete(c);
			} else {
				throw new IllegalArgumentException();
			}
		}
	}

	@Override
	public void deleteAll() {
		customer.clear();

	}

}
