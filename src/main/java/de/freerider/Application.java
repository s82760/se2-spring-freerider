package de.freerider;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;

import de.freerider.model.Customer;
import de.freerider.repository.CrudRepository;

@SpringBootApplication
public class Application {

    @Autowired
    CrudRepository<Customer, String> customerManager;

    @EventListener(ApplicationReadyEvent.class)
    public void doWhenApplicationReady() {

		Customer c1 = new Customer("Lillette", "Lilly", "Lilette@mail.de");
		Customer c2 = new Customer("Blobfish", "Blobby", "Blobstar@mail.com");
		Customer c3 = new Customer("Uila", "Erina", "Lilette@mail.de");
		Customer c4 = new Customer("Nigiri", "Nino", "nigirisushi@mail.de");
        Customer c5 = new Customer("Zaleski", "Zinian", "zmail@mail.de");
        
        Set<Customer> customer = new HashSet<>();
        customer.add(c2);
        customer.add(c3);
        customer.add(c4);
        customer.add(c5);

        c1.setId("1234");
        c2.setId("4545");
        c3.setId("5555");
        c4.setId("7896");
        c5.setId("6543");

        Set<String> idSet = new HashSet<>();
        idSet.add(c1.getId());
        idSet.add(c2.getId());
        idSet.add(c3.getId());
        idSet.add(c4.getId());
        idSet.add(c5.getId());


		System.out.println("save: " + customerManager.save(c1));
        System.out.println("saveAll: " + customerManager.saveAll(customer));
        System.out.println("findById: " + customerManager.findById("5555"));
        System.out.println("findAll: " + customerManager.findAll());
        System.out.println("findAllById: " + customerManager.findAllById(idSet));
        System.out.println("count: " + customerManager.count());
        System.out.println("delete: ");
        customerManager.delete(c1);
        System.out.println("deleteAllById: ");
        customerManager.deleteAllById(idSet);
        System.out.println("deleteAll: ");
        customerManager.deleteAll(customer);
        System.out.println("deleteAll: ");
        customerManager.deleteAll();
        System.out.println("Test komplett");
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

	}

}