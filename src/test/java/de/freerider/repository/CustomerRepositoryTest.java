package de.freerider.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import de.freerider.model.Customer;

@SpringBootTest
public class CustomerRepositoryTest {

	@Autowired
	CrudRepository<Customer,String> customerRepository;
	
	private Customer mats;
	private Customer thomas;
	private Customer lilly;
	
    Set<Customer> customer = new HashSet<>(); //Set mit customers
    Set<Customer> customernull = new HashSet<>(); //Set mit null Werten
	Set<String> idSet = new HashSet<>();
	
	@BeforeEach
	void createCustomer() {
		customerRepository.deleteAll();
		mats = new Customer("Matty", "Mats", "MatContact");
		thomas = new Customer("Thommy", "Thomas", "ThomContact");
		
		mats.setId("5555");
		thomas.setId(null);
		
	    customer.add(mats);
	    customer.add(thomas);
	    
	    customernull.add(null);
	    
	    idSet.add(mats.getId());
	}
	
	@Test
	void testConstructor() {
		assertEquals(0, customerRepository.count());
		assertFalse(customerRepository.findById(mats.getId()).isPresent()); 
		assertThrows(IllegalArgumentException.class, () ->{
			assertFalse(customerRepository.findById(thomas.getId()).isPresent());  
		});
	
		Set<Customer> c = (Set<Customer>) customerRepository.findAll();
		assertEquals(0, c.size());
	}
	
	@Test
	void save() {	
		//reguläre Fälle
		//2 Customer gespeichert
		customerRepository.save(mats);
		assertEquals(1, customerRepository.count());
		assertTrue(customerRepository.findById(mats.getId()).isPresent());
		customerRepository.save(thomas); 
		assertEquals(2, customerRepository.count());	
		assertTrue(customerRepository.findById(thomas.getId()).isPresent());
			
		assertNotNull(mats.getId());
		assertNotNull(thomas.getId()); //Id wurde generiert, statt null zurückzugeben
		
		//Sonderfälle
		//Customer mit selber id hinzufügen
		lilly = new Customer("Lilly", "Lillette", "LiContact");
		lilly.setId("5555");
		customerRepository.save(lilly);
		assertEquals(2, customerRepository.count());
		assertTrue(customerRepository.findById(lilly.getId()).isPresent());
		
		//kein leeren Customer hinzufügen
		assertThrows(IllegalArgumentException.class, () ->{
			customerRepository.save(thomas);
			assertEquals(2, customerRepository.count());
			
			customerRepository.save(null);
			assertEquals(2, customerRepository.count());
		});	
	}
	
	@Test
	void saveAll() {
			//reguläre Fälle
			customerRepository.saveAll(customer);
			assertEquals(2, customerRepository.count());
			
			//Sonderfälle
			assertThrows(IllegalArgumentException.class, () ->{
				customerRepository.saveAll(customernull);
				customerRepository.saveAll(null);
			});
				
			assertEquals(2, customerRepository.count());		
	}
	
	@Test
	void findAll() {
		//reguläre Fälle
		customerRepository.save(mats);
		customerRepository.save(thomas);
		Set<Customer> findCustomers = (Set<Customer>) customerRepository.findAll();
		assertEquals(2, findCustomers.size());
		
		//Sonderfall
		assertThrows(IllegalArgumentException.class, () ->{
			customerRepository.save(null);
		});
		assertEquals(2, findCustomers.size());
	}
	
	@Test
	void findById() {
		//reguläre Fälle
		customerRepository.save(mats);
		customerRepository.save(thomas);
		assertEquals(mats, customerRepository.findById(mats.getId()).get());
		assertEquals(thomas, customerRepository.findById(thomas.getId()).get());
		
		//Sonderfall: falls eine id null ist
		assertThrows(IllegalArgumentException.class, () ->{
			assertEquals(null, customerRepository.findById(null));
		});
		//id die nicht existiert
		assertFalse(customerRepository.findById("5647").isPresent());
		assertFalse(customerRepository.findById("").isPresent());
	}
	
	@Test
	void findAllById() {
		//regulärer Fall
		customerRepository.save(mats);
		customerRepository.save(thomas);
		idSet.add(mats.getId());
		idSet.add(thomas.getId());
		
		Set<Customer> c = (Set<Customer>) customerRepository.findAllById(idSet);
		assertEquals(2, c.size());
		
		//Sonderfälle
		idSet.add(null);
		assertThrows(IllegalArgumentException.class, () ->{
			customerRepository.findAllById(idSet);
		});
		assertThrows(IllegalArgumentException.class, () ->{
			customerRepository.findAllById(null);
		});
		assertEquals(2, c.size());
	}
	
	@Test
	void count() {
		//reguläre Fälle
		customerRepository.save(mats);
		assertEquals(1, customerRepository.count());
		customerRepository.save(thomas); 
		assertEquals(2, customerRepository.count());
		customerRepository.delete(thomas); 
		assertEquals(1, customerRepository.count());
		
		//Sonderfälle
		assertThrows(IllegalArgumentException.class, () ->{
			customerRepository.save(null); 
		});
		assertEquals(1, customerRepository.count());
		
		assertThrows(IllegalArgumentException.class, () ->{
			customerRepository.delete(null); 
		});
		assertEquals(1, customerRepository.count());
	}
	
	@Test
	void deleteById() {		
		//reguläre Fälle
		customerRepository.save(mats);
		assertEquals(1, customerRepository.count());
		customerRepository.deleteById("5555"); 
		assertEquals(0, customerRepository.count());
		
		//Sonderfälle
		assertThrows(IllegalArgumentException.class, () ->{
			customerRepository.deleteById(null); 
		});
		//id die nicht existiert
		customerRepository.save(mats);
		assertEquals(1, customerRepository.count());
		customerRepository.deleteById("8898"); 
		assertEquals(1, customerRepository.count());
	}
	
	@Test
	void delete() {
		//regulärer Fall
		customerRepository.save(mats);
		assertEquals(1, customerRepository.count());
		customerRepository.save(thomas); 
		assertEquals(2, customerRepository.count());
		customerRepository.delete(thomas); 
		assertEquals(1, customerRepository.count());
		customerRepository.delete(mats); 
		assertEquals(0, customerRepository.count());
		
		//Sonderfälle
		assertThrows(IllegalArgumentException.class, () ->{
			customerRepository.delete(null); 
		});
	}
	
	@Test
	void deleteAllById() {
		//reguläre Fälle
		customerRepository.save(mats);
		assertEquals(1, customerRepository.count());
		customerRepository.deleteAllById(idSet);
		assertEquals(0, customerRepository.count());
		
		//Sonderfälle
		customerRepository.save(mats);
		assertEquals(1, customerRepository.count());
		assertThrows(IllegalArgumentException.class, () ->{
			customerRepository.deleteAllById(null);  
		});
		assertEquals(1, customerRepository.count());
	}

	@Test
	void deleteAllIterableEntities() {
		//reguläre Fälle
		customerRepository.saveAll(customer);
		assertEquals(2, customerRepository.count());
		customerRepository.deleteAll(customer);
		assertEquals(0, customerRepository.count());
		
		//Sonderfälle
		assertThrows(IllegalArgumentException.class, () ->{
			customerRepository.deleteAll(null); 
		});
		assertThrows(IllegalArgumentException.class, () ->{
			customerRepository.deleteAll(customernull); 
		});
		assertEquals(0, customerRepository.count());
	}
	
	@Test
	void deleteAll() {
		assertEquals(0, customerRepository.count());
		customerRepository.save(mats);
		customerRepository.save(thomas);
		assertEquals(2, customerRepository.count());
		customerRepository.deleteAll();
		assertEquals(0, customerRepository.count());
	}
}
