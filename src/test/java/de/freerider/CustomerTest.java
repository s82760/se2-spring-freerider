package de.freerider;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.freerider.model.Customer;
import de.freerider.model.Customer.Status;

class CustomerTest {
	
	private Customer mats;
	private Customer thomas;
	
	@BeforeEach
	void createCustomer() {
		mats = new Customer();
		thomas = new Customer("Müller", "Thomas", "FußballContact");
	}
	
	@Test
	void dataThomas() {
		assertEquals(thomas.getfirstName(), "Thomas");
		assertEquals(thomas.getlastName(), "Müller");
		assertEquals(thomas.getcontact(), "FußballContact");
		assertEquals(thomas.getId(), null);
	}
	
	@Test
	void dataMats() {
		assertEquals(mats.getfirstName(), "");
		assertEquals(mats.getlastName(), "");
		assertEquals(mats.getcontact(), "");
		assertEquals(mats.getId(), null);
	}
	
	@Test
	void testIdNull() {
		assertNull(mats.getId());
		assertNull(thomas.getId());
	}
	
	
	@Test
	void testSetId() {
		mats.setId("1235");
		assertNotNull(mats.getId());
		
		thomas.setId("5689");
		assertNotNull(thomas.getId());
	}
	
	@Test
	void testSetIdOnlyOnce() {
		mats.setId("1235");
		assertEquals("1235", mats.getId());
		mats.setId("7859");
		assertEquals("1235", mats.getId());
		
		thomas.setId("1255");
		assertEquals("1255", thomas.getId());
		thomas.setId("78459");
		assertEquals("1255", thomas.getId());
	}
	
	@Test
	void testResetId() {
		mats.setId("1235");
		assertEquals("1235", mats.getId());
		mats.setId(null);
		assertNull(mats.getId());
		
		thomas.setId("1255");
		assertEquals("1255", thomas.getId());
		thomas.setId(null);
		assertNull(thomas.getId());
	}
	
	@Test
	void testNamesInitial() {
		assertEquals("",mats.getlastName());
		//assertNotNull(mats.getlastName());
		assertEquals("",mats.getfirstName());
		//assertNotNull(mats.getfirstName());
	
	}

	@Test
	void testNamesSetNull() {
		mats.setfirstName(null);
		mats.setlastName(null);
		assertEquals("", mats.getlastName());
		//assertNotNull(mats.getlastName());
		assertEquals("", mats.getfirstName());
		//assertNotNull(mats.getfirstName());
		
		thomas.setfirstName(null);
		thomas.setlastName(null);
		assertEquals("", thomas.getlastName());
		//assertNotNull(thomas.getlastName());
		assertEquals("", thomas.getfirstName());
		//assertNotNull(thomas.getfirstName());
	}
	
	@Test
	void testSetNames() {
		mats.setfirstName("matsfirstname");
		mats.setlastName("matslastname");
		assertEquals("matslastname", mats.getlastName());
		assertEquals("matsfirstname", mats.getfirstName());
		
		thomas.setfirstName("thomasfirstname");
		thomas.setlastName("thomaslastname");
		assertEquals("thomaslastname", thomas.getlastName());
		assertEquals("thomasfirstname", thomas.getfirstName());
	}
	
	@Test
	void testSetContactInitial() {
		assertEquals(mats.getcontact(), "");
		//assertNotNull(mats.getcontact());

	}
	
	@Test
	void testContactSetNull() {
		mats.setcontact(null);
		assertEquals(mats.getcontact(), "");
		//assertNotNull(mats.getcontact());
		
		thomas.setcontact(null);
		assertEquals(thomas.getcontact(), "");
		//assertNotNull(thomas.getcontact());
	}
	
	@Test
	void testSetContact() {
		mats.setcontact("matscontact");
		assertEquals(mats.getcontact(), "matscontact");
		
		thomas.setcontact("thomascontact");
		assertEquals(thomas.getcontact(), "thomascontact");
	}
	
	@Test
	void testStatusInitial() {
		assertEquals(Status.New, mats.getStatus());
		assertEquals(Status.New, thomas.getStatus());
	}
	
	@Test
	void testSetStatus() {
		mats.setStatus(Status.Active);
		assertEquals(Status.Active, mats.getStatus());
		
		thomas.setStatus(Status.Suspended);
		assertEquals(Status.Suspended, thomas.getStatus());
	}

}
